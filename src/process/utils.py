from datetime import timedelta
from typing import List, Tuple

import numpy as np
import pandas
import pandas as pd

from setup.database import db_con


db = db_con()


def color_risk(val: str):
    """
    Maps dataframe column with risk colors
    
    Args:
        val: Risk value
    """
    if val == "Low":
        color = "rgba(0,128,0,0.4)"
    if val == "Med":
        color = "rgba(255,255,0,0.4)"
    if val == "High":
        color = "rgba(255,0,0,0.4)"
    return f"background-color: {color}"


def find_asset_info(target: str) -> Tuple[str, str, str]:
    """
    For a given CSO, finds the type, latitude and longitude.

    Args:
        target: Target CSO

    Returns:
        Tuple of cso type, latitude, longitude
    """
    df = pd.DataFrame(
        db["WESSEX_Site_Info"].find(
            {}, {"_id": 0, "Site_ID": 1, "type": 1, "lat": 1, "lon": 1}
        )
    )

    cso_type = df[df["Site_ID"] == target]["type"].values[0]
    cso_lat = round(df[df["Site_ID"] == target]["lat"].values[0], 5)
    cso_lon = round(df[df["Site_ID"] == target]["lon"].values[0], 5)
    return cso_type, cso_lat, cso_lon


def find_sites() -> Tuple[List[str], List[str]]:
    """
    Find the sites that have level data available i.e. not a WRC or pumping station.

    Returns:
        Tuple of list of CSO sites that have data, and list of sites that have no data
    """
    site_info = pd.DataFrame(db["WESSEX_Site_Info"].find({}, {"_id": 0, "Site_ID": 1}))
    levels = pd.DataFrame(
        db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find_one({}, {"_id": 0}), index=[0]
    ).set_index("Datetime")
    sites_with_no_data = list(set(site_info["Site_ID"]) - set(levels.columns))
    sites_with_data = [x for x in levels.columns if x not in sites_with_no_data]
    return sites_with_data, sites_with_no_data


def find_cso_cluster(target: str) -> Tuple[str, str, List[str]]:
    """
    For a given CSO, returns the upstream, downstream CSOs and total list of CSOs

    Args:
        target (str): Target CSO

    Returns:
        Tuple of upstream CSO, downstream CSO, list of upstream/downstream/target
    """
    df = pd.DataFrame(
        db["WESSEX_Site_Info"].find({}, {"_id": 0, "Site_ID": 1, "down_stream": 1})
    )

    try:
        upstream = df[df["down_stream"] == target]["Site_ID"].values.tolist()
        downstream = df[df["Site_ID"] == target]["down_stream"].values.tolist()
        cso_chain = [[target], upstream, downstream]
    except IndexError:
        try:
            upstream = "No upstream CSO exists."
            downstream = df[df["Site_ID"] == target]["down_stream"].values.tolist()
            cso_chain = [[target], downstream]
        except IndexError:
            upstream = df[df["down_stream"] == target]["Site_ID"].values.tolist()
            downstream = "No downstream CSO exists."
            cso_chain = [[target], upstream]

    cso_cluster = [item for sublist in cso_chain for item in sublist]

    sites_with_data, sites_with_no_data = find_sites()
    cso_cluster = [x for x in cso_cluster if x not in sites_with_no_data]
    upstream = [x for x in upstream if x not in sites_with_no_data]
    downstream = [x for x in downstream if x not in sites_with_no_data]
    return upstream, downstream, cso_cluster


def process_result(
    target: str, start_time: str
) -> Tuple[pandas.core.frame.DataFrame, List[str]]:
    """
    Takes the target and start time, then finds the Prophet predictions that have been stored in the database
    Using the Prophet generated bands, scaled by a factor, compares real vs predicted values
    Indicates points in time where real/predicted diverge
    Produces a dataframe that summarizes theses results

    Args:
        target: Target CSO
        start_time: Star time for analysis
    
    Returns:
        Tuple containing results dataframe and a list of alarms that would have triggered
    """
    band_scaler = 2
    start_time = pd.to_datetime(start_time)

    query = {
        "Datetime": {
            "$gte": start_time - timedelta(hours=24 * 7),
            "$lte": start_time - timedelta(hours=1),
        }
    }
    project = {"_id": 0, "Datetime": 1, target: 1}
    real_df = (
        pd.DataFrame(db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find(query, project))
        .set_index("Datetime")
        .sort_index()
    )

    query = {
        "Datetime": {
            "$gte": start_time - timedelta(hours=24 * 7),
            "$lte": start_time - timedelta(hours=1),
        }
    }
    project = {
        "_id": 0,
        "Datetime": 1,
        f"{target}_yhat": 1,
        f"{target}_yhat_upper": 1,
        f"{target}_yhat_lower": 1,
    }
    res_df = (
        pd.DataFrame(
            db["WESSEX_E_Numbers_Apr_2019_60Min_Mean_Prophet_Predictions"].find(
                query, project
            )
        )
        .set_index("Datetime")
        .sort_index()
    )
    res_df["y"] = real_df[target]
    res_df = res_df.reset_index()

    res_df[f"{target}_yhat_upper_raw"] = res_df[f"{target}_yhat_upper"]
    res_df[f"{target}_yhat_lower_raw"] = res_df[f"{target}_yhat_lower"]

    res_df[f"{target}_yhat_upper"] = (
        res_df[f"{target}_yhat"]
        + (res_df[f"{target}_yhat_upper_raw"] - res_df[f"{target}_yhat"]) * band_scaler
    )
    res_df[f"{target}_yhat_lower"] = (
        res_df[f"{target}_yhat"]
        - (res_df[f"{target}_yhat"] - res_df[f"{target}_yhat_lower_raw"]) * band_scaler
    )

    res_df = res_df.rename(
        columns={
            "Datetime": "ds",
            f"{target}_yhat": "yhat",
            f"{target}_yhat_upper": "yhat_upper",
            f"{target}_yhat_lower": "yhat_lower",
        }
    )
    res_df = res_df.set_index("ds")
    res_df["alarm"] = np.where(
        ((res_df.y < res_df.yhat_lower) | (res_df.y > res_df.yhat_upper)), 1, 0,
    )
    alarms_list = res_df[res_df["alarm"] == 1].index
    return res_df, alarms_list
