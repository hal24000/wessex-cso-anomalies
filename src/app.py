import streamlit as st

from pages.page_main import run_page_main
from setup.layout import setup_page

setup_page("Network Anomaly Finder")

run_page_main()

with st.sidebar:
    st.header("")
    st.info(
        """
    ### :information_source: Info
    - Anomaly detection for CSO networks.
    - Creates prediction for past and future water levels/flow, which are used to detect anomalies.
    """
    )
