# Sensor Network Anomaly Detection

Anomaly detection framework for CSO network.

## Usage
```bash
$ cd wessex-cso-anomaly/src
$ streamlit run app.py
```

## Project Organisation

    ├── chart                        <- Files required for Dimensionops build
    │
    ├── docs                         <- Documentation
    │
    ├── notebooks                    <- Jupyter notebooks
    │
    ├── src                          <- Source code for project
    │   │
    │   │── model                    <- Model
    │   │   └── model.py             <- Model code
    │   │
    │   │── pages                    <- Pages
    │   │   └── page_main.py         <- Main page of app
    │   │
    │   │── setup                    <- Setup
    │   │   │── database.py          <- Database connection
    │   │   │── favicon.ico          <- Icon
    │   │   └── layout.py            <- Layout settings
    │   │
    │   └── app.py                   <- App run file
    │   
    │── Dockerfile                   <- File to assemble a Docker image
    │
    │── environment.yml              <- Environment yml file to create conda environment
    │
    ├── README.md                    <- README for this project
    │
    └── requirements.txt             <- Requirements file for creating app environment